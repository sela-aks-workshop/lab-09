# Kubernetes Workshop
Lab 09: Deploying Prometheus using Helm

---

## Instructions

### Ensure that your environment is clean

 - List existent deployments
```
kubectl get all
```

### Deploy Prometheus using Helm Packages

 - Download Helm Client
```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > ~/get_helm.sh
```

 - Install Helm Client
```
chmod +x ~/get_helm.sh
~/get_helm.sh
```

 - Create a service account for Tiller
```
cat << EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
  namespace: kube-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
```

 - Configure Helm
```
helm init --service-account tiller
```

 - Deploy Prometheus using helm
```
helm install stable/prometheus --name prometheus --set server.service.type=LoadBalancer
```

 - See all the helm releases deployed to the cluster
```
helm ls
```

 - List existent pods
```
kubectl get pods
```

 - List existent services
```
kubectl get services
```

 - Browse to prometheus and check the configured targets (surprise, everything is already configured!)
```
http://<service-ip>/targets
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete the installed chart
```
helm delete prometheus
```

 - List existent resources
```
kubectl get all
```
